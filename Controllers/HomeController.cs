using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Security.Claims;

namespace Basic.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        // are you allow?
        [Authorize]
        public IActionResult Secret()
        {
            return View();
        }

        public IActionResult Authentication()
        {
            // create basic user claims
            var claims = new List<Claim>()
            {
                new Claim(ClaimTypes.Name, "Bob"),
                new Claim(ClaimTypes.Email, "bob@fmail.com"),
                new Claim("Grandma.Says","very very nice boy")
            };

            // create lisence for user
            var licenceClaims = new List<Claim>() {
                new Claim(ClaimTypes.Name, "Bob K Foo"),
                new Claim("DrivingLicense","A+")
            };

            // create user identity
            var identity = new ClaimsIdentity(claims, "Grandma Identity");

            // create lisence identity for user
            var licenseIdentity = new ClaimsIdentity(licenceClaims, "Gorverment");

            // create user principal and set user identities
            var userPrincipal = new ClaimsPrincipal(new[] { identity, licenseIdentity });

            // add principal to http , so user singed in
            HttpContext.SignInAsync(userPrincipal);

            return RedirectToAction(nameof(Secret));
        }
    }
}